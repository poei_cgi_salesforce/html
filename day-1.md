## **Langage de balisage :**

Langage avec des balises. 
Une balise ouvrante correspond à une balise fermante il faut absolument les fermer sauf balise auto-fermante.
Une balise peut avoir des attributs
````xml
<?xml version="1.0"?>
  <classroom attribut="valeur">
    ...
    </classroom>
````



## DTD : ##

Permet de définir la structure d'un code XML.

Exemple de déclaration d'une DTD 

````xml
<!-- 1) Ajout du doctype -->
<!-- 1) A l'interieur on décrit nos elements -->
 <!DOCTYPE classroom [
            <!ELEMENT classroom (message)>
            <!ELEMENT message (#PCDATA)>
        ]>
<classroom>
    <message>hello</message>
</classroom>
````

Pour éviter que quelqu'un forme un XML non valide. 
Peut servir de documentation

##  SVG :
Images vectorielles. On peut zoomer à l'infinie pas de pixellisation. On l'utilise pour les logos 

##  XML :
On peut l'utiliser pour stocker des données. Utiliser également pour de la configuration. 
Pour écrire du xml on utilise le doctype 


````xml
<?xml version="1.0"?>
````

On ajoute des balises
````xml
<?xml version="1.0"?>
  <classroom>
    ...
    </classroom>
````



## HTML ## 
Langage de description. Pas un langage de programmation.
langage de balise

##  HTTP : ##

Protocole utilisé par le web
HTTPS dans sa version sécurisée
Un client (navigateur web) envoie une requête HTTP. Celui ci renvoie une réponse HTTP 

Nous avons dans une requête HTTP 
- Headers (envoie des informations du client dans la requête)
- Methode HTTP (GET, POST, PUT, ...). Notre navigateur envoie des requêtes de type GET pour afficher un site web. POST quand on soumet un formulaire
- URL = Adresse de la ressource sur laquelle on envoie la requête
- body (contenu de la requête exemple -> saisie de formulaire)

Nous avons dans une réponse

- Code réponse. 
  Ceux qui commencent par un 2 -> succes
  Ceux qui commencent par un 3 -> redirections
  Ceux qui commencent par un 4 -> erreurs client
  Ceux qui commencent par un 5 -> erreurs serveur
  
- Headers sont envoyés par les serveurs et décrivent le contenu de la réponse pour que le client l'affiche bien

- Body
  Code en lui même il sera interprété par le navigateur 


##  Ouvrir un document html : ##

Ecrit dans un document avec l'extension .html 

On mettant le doctype HTML puis les balises HTML

````html
<!DOCTYPE html>
<html lang="fr">
</html>
````

##  BALISE HTML HEAD : ##

Balise qui n'est pas visible sur notre site. Elle est utile pour les moteurs de recherche et pour notre navigateur

Balise title : représente le titre de la page (moteur de recherche / onglet du navigateur)

Elle est affichée en dessous du titre
La meta description par exemple est affichée dans les resultats google / bing / ...

Meta charset : Permet de spécifier l'encodage de la page (caractères spéciaux)

````html
<head>
     <meta charset="UTF-8">
</head>
````
##  BALISE HTML HEAD : ##

Contient le contenu affiché sur la page du navigateur 
(titres, images, videos, text ..)

##  Titres : ##

Différents titres 

De h1 à h6. On respecte une structure logique (du plus petit au plus grand, un seul H1). Il ne faut pas penser en terme de grosseur d'affichage mais en terme d'importance de l'information (utilisé SEO moteur e recherche)

````html
<body>
<h1>News</h1>
<h2>Sport</h2>
<h3>Football</h3>
<h2>Faits divers</h2>
</body>
````


##  DIV Container : ##

Sert à cibler une partie de la page (ajouter des attributs)
Correspond à une partie de l'affichage

On peut les imbriquer et on pourra les selectionner ensuite en fonction de la structure

##  Liens : ##

Ils sont contenu dans une balises A. Permet de faire un encrage dans la page. Il contient une URL dans l'attribut href

Peut contenir une URL complète (http://www.google.com) / Utilisation d'une url absolue (ressources sur le web. Attention elle peut être modifiée par quelqu'un d'autre)

Peut contenir l'adresse d'un nouveau fichier sur le serveur / Utilisation d'une url relative (ressource sur votre serveur. vous appartient)

````html
<a href="http://google.com">Aller sur google</a>
<a href="mapage.html">Aller sur la page mapage.html</a>
````

##  ol / ul / li ##

ol : se matérialise par un numéro liste ordonnée
ul : Liste non ordonnée on aura des puces

````html
<ul>
    <li>Element 1</li>
    <li>Element 2</li>
</ul>

<ol>
    <li>Element 1</li>
    <li>Element 2</li>
</ol>
````


##  Caractères spéciaux ##

2 manières de faire (utf-8, en utilisant le code du caractère).
On utilise le &...; représente le code d'un caractère
On peut utiliser le code avec un encodage en utf-8 (intéret limité)

