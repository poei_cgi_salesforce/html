##  HTML DAY 2 : ##

## Liens / Ancres ##
Liens : Permet d'envoyer vers une autre page de son site ou un autre site internet

Ancres : Fais un lien vers une div spécifique dans la page (div, section , ...)

````html
    <!-- Premierement, il faut identifier notre élément avec un id -->
    <div id="monAncre"></div>

    <!-- On utilise un lien classique en utilisant href puis un "#" puis l'id de la div -->
    <a href="#monAncre">Lien vers mon ancre</a>

    <a href="https://www.leboncoin.fr/nautisme/2292701709.htm#map">Lien vers la map</a>
    
````


Liens spéciaux : 

- target="_blank"
- title : Titre du lien. Permet lors du survol par la souris d'afficher le contenu de la balise
- mailto : Permet d'envoyer un mail / Idem tel, skype ...
- download : Permet de télécharger un fichier

````html

<!-- Ceci est en lien -->
<a title="Lien vers google" href="http://www.google.com">Lien vers google</a>
<!-- Ceci est un lien dans un nouvel onglet -->
<a title="Lien vers google" target="_blank" href="http://www.google.com">Lien vers google</a>
<!-- Ceci est un lien qui envoie un mail -->
<a href="mailto:adelorme-ext@formateur-humanbooster.com">Send me a mail !</a>
<!-- Ceci est un lien qui télécharge un fichier -->
<a href="cv.pdf" download>Télécharger mon CV</a>
````


## form ##

Envoyer des données au serveur (saisie utilisateur)

Attribut method get/post (get : on chercher à obtenir une ressource (page web), post : on envoie une ressource)
Dans le cadre des tes formulaires, nous utilisons POST 
Attribut action : sert à spécifier la page vers laquelle on envoie les donnees. Pour l'instant on utilise #
On peut utiliser la méthode GET dans certains cas précis (permet de faire passer des paramètres dans l'url)
placeholder : permet d'afficher dans le bouton une suggestion pour indiquer ce qu'il faut saisir
value : permet de présaisir le champ. On a vraiment de la donnée dedans (utiliser sur les formulaires d'édition)
On peut associer un Label à un input. Cela permet de les associer gràce à un id coté input et un for cota label

````html
<label for="input">Label</label>
<input type="text" name="input_text" id="input">
````

Plusieurs types différents : 

- Saisie de text

````html
<input type="text" name="input_text">
````
- Saisie de nombre

````html
<input type="number" name="input_number">
````

- Saisie de date
```html
<input type="date" name="date_input">
```
- Checkbox
Permet de faire des choix multiples grâce à des cases à cocher
````html
<input type="checkbox" name="valeur" value="cle">
<input type="checkbox" name="valeur2" value="cle2">
````



- Radio 
Idem que la checkbox mais on peut selectionner une seule valeur
attribut checked : permet de selectionner une valeur par défaut  
  
````html
<input type="radio" name="gender" value="M"><label>Men</label>
<input type="radio" name="gender" value="W"><label>Women</label>
````

- Select
Permet de selectionner une valeur dans une liste 
Dans cette balise on ajoute autant de balise action que nécessaire
attribut selected : permet de selectionner une valeur par défaut
Dans le cas ci-dessous, l'option 2 sera selectionné de base
````html
    <select name="select_input">
        <option value="opt1">Option 1</option>
        <option value="opt2" selected>Option 2</option>
    </select>
````

- Mot de passe 
Permet de masquer une saisie utilisateur dans un input. Utile pour la saisie / confirmation de mots de passes
  
````html
    <input type="password" name="my_password">
````

## Table ##
Permet d'afficher un tableau. 

On utilise les balises table

````html
    <table>
    
    </table>
````

Composé de 2 parties (thead et tbody)
Dans thead, les titres des colonnes.
Dans tbody, on affiche les données de chaque ligne

````html
    <table>
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>
````



Les balises tr (table row) représentent une ligne
Les balises th sont les colonnes du tableau (thead)

````html
    <table>
        <thead>
            <th></th>
        </thead>
        <tbody>
            <tr></tr>
        </tbody>
    </table>
````



Les balises td (table data) permettent de créer une cellule. Une balise td peut contenir divers éléments (images, vidéo, ...)
````html
    <table>
        <thead>
            <th>Entete colonne 1</th>
        </thead>
        <tbody>
            <tr>
                <td>Cellule 1</td>
            </tr>
        </tbody>
    </table>
````

## Image ##

Utilisation de la balise img 
On a un attribut src : source de l'image (sur internet ou sur mon serveur)
On peut la redimensionner (attributs length / width)
On peut les combiner avec des liens 
attribut alt : sert a afficher du texte si l'image ne charge pas 
attribut title : sert pour les personnes mal voyante permet de décrire l'image (affiché au survol de la souris). Aussi important SEO

````html
<img src="images/mon-image.jpg" alt="Une image" title="Titre de l'image">
````

## Audio ##
Permet de lancer une musique dans la page web
Utilisation de la balise <audio>
On peut ajouter des controles (permet de mettre de mettre pause / play ...)
autoplay Lance automatiquement la piste audio
loop Lance en boucle
On peut mettre plusieurs source (dans le cas ou le navigateur ne peut pas lire un format). 

```html
<audio controls autoplay loop>
  <source src="myAudio.mp3" type="audio/mpeg">
  <source src="myAudio.ogg" type="audio/ogg">
  <p>Votre navigateur ne prend pas en charge l'audio HTML5.
  Voici un <a href="myAudio.mp3">lien vers le fichier audio</a>
  à la place.</p>
</audio>
```
## Vidéo ##

Idem que l'audio sauf que l'on utilise la balise video 

```html
<video controls autoplay loop>
  <source src="myAudio.mp3" type="audio/mpeg">
  <source src="myAudio.ogg" type="audio/ogg">
  <p>Votre navigateur ne prend pas en charge l'audio HTML5.
  Voici un <a href="myAudio.mp3">lien vers le fichier audio</a>
  à la place.</p>
</video>
```